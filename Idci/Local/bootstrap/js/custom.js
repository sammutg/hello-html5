$(document).ready(function(){

/////////////////////////////
// toggleFullScreen
/////////////////////////////

    //$('a.[href="toggleFullScreen"').click(function(event) {
    $('#toggleFullScreen').click(function(event) {
        event.preventDefault();
        toggleFullScreen();
    });


/////////////////////////////
// Localisation
/////////////////////////////

      var defaultLocation = $('#map-output').attr('data-default-location');
      var defaultZoom = $('#map-output').attr('data-default-zoom');
      var defaultMaptype = $('#map-output').attr('data-default-maptype');
      var map = initialize(defaultLocation, defaultZoom, defaultMaptype);
      var me = null;

      // function initialize(location, zoom, maptype) {
      // console.log(location);
      // console.log(zoom);
      // console.log('google.maps.MapTypeId.'+maptype);

      //   var mapOptions = {
      //     center: new google.maps.LatLng(-34.397, 150.644),
      //     zoom: parseInt(zoom),
      //     mapTypeId: eval('google.maps.MapTypeId.'+maptype)
      //   };
      //   var map = new google.maps.Map(
      //     document.getElementById("map-output"),
      //     mapOptions
      //   );

      // };

      function initialize(location, zoom, maptype) {
        console.log(location);
        console.log(zoom);
        console.log('google.maps.MapTypeId.'+maptype);
        console.log(map);

        var geocoder = new google.maps.Geocoder();
        var map = null;

        geocoder.geocode( { 'address': location}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            console.log(results[0].geometry.location);

            var mapOptions = {
              center: results[0].geometry.location,
              zoom:parseInt(zoom),
              mapTypeId: eval('google.maps.MapTypeId.'+maptype)
            };
            
            var map = new google.maps.Map(
              document.getElementById("map-output"),
              mapOptions
            );
            // var marker = new google.maps.Marker({
            //     position: results[0].geometry.location,
            //     map: map
            // });

          } else {
              alert('Error: geocoder '+status);
          }
        });

          return map;

      }

      /////////////////////////////
      // Bouton de localisation
      /////////////////////////////
      $('#locate-me').click(function(event) {
        event.preventDefault();
         // TODO : localisation
        if (navigator.geolocation) {
          navigator.geolocation.watchPosition(function(position) {
            console.log(position);
            var me = new google.maps.Marker({
                position: new google.maps.LatLng(
                  position.coords.lattitude,
                  position.coords.longitude
                ),
                map: map,
                title: $('input[name="username"]').val()
            });

          });
        };
      });


      /////////////////////////////
      // Canvas
      /////////////////////////////
        var canvas = document.getElementById('my-draw-area');
        var width = $('#main > .content').width();
        var height = $('#main > .content').height();
        console.log(width);
        console.log(height);
        canvasManager = new CanvasManager(canvas, width, height);
        canvasManager.init();



// eof document.ready
});

function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    // alternative standard method
      (!document.mozFullScreenElement && !document.webkitFullScreenElement)) {  // current working methods
    if (document.documentElement.requestFullScreen) {
      document.documentElement.requestFullScreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullScreen) {
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}



