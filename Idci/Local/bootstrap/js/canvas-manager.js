
// var CanvasManager = function(canvas, width, height) {

//   this.canvas = canvas;
//   this.width  = width;
//   this.height = height;
//   this.ctx    = null;

//   this.init = function(color) {
//     if (!canvas.getContext) {
//       return false;
//     }

//     if(color == undefined) {
//       color = '#FF0000';
//     }

//     this.canvas.width = this.width;
//     this.canvas.height = this.height;
//     this.ctx = this.canvas.getContext('2d');
//     this.ctx.fillStyle=color;
//     this.ctx.fillRect(0,0,this.width,this.height);

//     return true;
//   };

// };

var CanvasManager = function(canvas, width, height, color) {

  this.canvas       = canvas;
  this.width        = width;
  this.height       = height;
  this.color        = (color == undefined) ? '#EEE' : color;
  this.ctx          = null;
  this.currentColor = "#000000";
  this.currentSize  = 1;
  // Last down event
  this.lde          = null;
  this.state        = '';

  this.init = function() {
    if (!canvas.get(0).getContext) {
      return false;
    }

    this.canvas.get(0).width = this.width;
    this.canvas.get(0).height = this.height;
    this.ctx = this.canvas.get(0).getContext('2d');
    this.clear();

    this.toolListeners();
    this.canvasListeners();

    return true;
  };

  this.setCurrentColor = function(color) {
    this.currentColor = color;
  }

  this.setCurrentSize = function(size) {
    this.currentSize = size;
  }

  this.setState = function(state) {
    this.state = state;
  }

  this.clear = function() {
    this.ctx.beginPath();
    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(0, 0, this.width, this.height);
  };

  this.getCurrentTool = function() {
    return this.canvas.attr('data-current-tool');
  };

  this.storeLastDownEvent = function(event) {
    this.lde = event;
  }

  this.drawLine = function(lue) {
    from = { x: this.lde.offsetX, y: this.lde.offsetY };
    if(this.lde.offsetX == undefined) {
      // Firefox bug
      from = {
        x: this.lde.pageX-this.canvas.offset().left,
        y: this.lde.pageY-this.canvas.offset().top
      };
    }

    to = { x: lue.offsetX, y: lue.offsetY };
    if(lue.offsetX == undefined) {
      // Firefox bug
      to = {
        x: lue.pageX-this.canvas.offset().left,
        y: lue.pageY-this.canvas.offset().top
      };
    }

    this.ctx.beginPath();
    this.ctx.strokeStyle = this.currentColor;
    this.ctx.lineWidth = this.currentSize;
    this.ctx.moveTo(from.x, from.y);
    this.ctx.lineTo(to.x, to.y);
    this.ctx.stroke();
    this.ctx.closePath();
  };

  this.drawPencil = function(lue) {
    from = { x: this.lde.offsetX, y: this.lde.offsetY };
    if(this.lde.offsetX == undefined) {
      // Firefox bug
      from = {
        x: this.lde.pageX-this.canvas.offset().left,
        y: this.lde.pageY-this.canvas.offset().top
      };
    }

    to = { x: lue.offsetX, y: lue.offsetY };
    if(lue.offsetX == undefined) {
      // Firefox bug
      to = {
        x: lue.pageX-this.canvas.offset().left,
        y: lue.pageY-this.canvas.offset().top
      };
    }

    this.ctx.save();
    this.ctx.beginPath();
    this.ctx.lineCap = "round";
    this.ctx.strokeStyle = this.currentColor;
    this.ctx.lineWidth = this.currentSize;
    this.ctx.moveTo(from.x, from.y);
    this.ctx.lineTo(to.x, to.y);
    this.ctx.stroke();
    this.ctx.closePath();
    this.ctx.restore();
  };

  this.toolListeners = function() {
    var manager = this;
    $('#drawing-tools > a').click(function(event){
      event.preventDefault();
      if($(this).attr('id') == 'clear') {
        manager.canvas.attr('data-current-tool', '');
        manager.clear();
      } else {
        var selectedActionTool = $(this).attr('id');
        if(manager.getCurrentTool() != selectedActionTool) {
          // A new tool is selected
          manager.canvas.attr('data-current-tool', selectedActionTool);
        } else {
          // A cancel tool is clicked
          manager.canvas.attr('data-current-tool', '');
        }
      }
    });
    $('#drawing-tools > #color').change(function(event){
      console.log($(this).val());
      manager.setCurrentColor($(this).val());
    });
    $('#drawing-tools > #size').change(function(event){
      console.log($(this).val());
      manager.setCurrentSize($(this).val());
    });
  };

  this.canvasListeners = function() {
    var manager = this;

    this.canvas.mouseup(function(e) {
      manager.setState('up');
      if(manager.getCurrentTool() != '') {
        if(manager.getCurrentTool() == 'line') {
          manager.drawLine(e);
        } else if(manager.getCurrentTool() == 'pen') {
          manager.drawPencil(e);
        }
      }
    });

    this.canvas.mousedown(function(e) {
      manager.setState('down');
      if(manager.getCurrentTool() != '') {
        manager.storeLastDownEvent(e);
      }
    });

    this.canvas.mousemove(function(e) {
      if(manager.getCurrentTool() == 'pen' && manager.state == 'down') {
        manager.drawPencil(e);
        manager.storeLastDownEvent(e);
      }
    });
  };
};
