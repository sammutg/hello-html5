var CanvasManager = function(canvas, width, height, color, socket) {

  this.canvas       = canvas;
  this.width        = width;
  this.height       = height;
  this.color        = (color == undefined) ? '#EEE' : color;
  this.socket       = socket;
  this.ctx          = null;
  this.currentColor = "#000000";
  this.currentSize  = 1;
  // Last down event
  this.lde          = null;
  this.state        = '';

  this.init = function() {
    if (!canvas.get(0).getContext) {
      return false;
    }

    this.canvas.get(0).width = this.width;
    this.canvas.get(0).height = this.height;
    this.ctx = this.canvas.get(0).getContext('2d');
    this.clear();

    this.toolListeners();
    this.canvasListeners();
    this.remoteListeners();

    return true;
  };

  this.setCurrentColor = function(color) {
    this.currentColor = color;
  }

  this.setCurrentSize = function(size) {
    this.currentSize = size;
  }

  this.setState = function(state) {
    this.state = state;
  }

  this.clear = function() {
    layoutClear(this.ctx, this.color, this.width, this.height);
    this.socket.emit('clear', {});
  };

  this.getCurrentTool = function() {
    return this.canvas.attr('data-current-tool');
  };

  this.storeLastDownEvent = function(event) {
    this.lde = event;
  }

  this.drawLine = function(lue) {
    from = { x: this.lde.offsetX, y: this.lde.offsetY };
    if(this.lde.offsetX == undefined) {
      // Firefox bug
      from = {
        x: this.lde.pageX-this.canvas.offset().left,
        y: this.lde.pageY-this.canvas.offset().top
      };
    }

    to = { x: lue.offsetX, y: lue.offsetY };
    if(lue.offsetX == undefined) {
      // Firefox bug
      to = {
        x: lue.pageX-this.canvas.offset().left,
        y: lue.pageY-this.canvas.offset().top
      };
    }

    layoutDrawLine(this.ctx, from, to, this.currentColor, this.currentSize);

    this.socket.emit('drawLine', {
      'from': from,
      'to': to,
      'color': this.currentColor,
      'size': this.currentSize
    });
  };

  this.drawPencil = function(lue) {
    from = { x: this.lde.offsetX, y: this.lde.offsetY };
    if(this.lde.offsetX == undefined) {
      // Firefox bug
      from = {
        x: this.lde.pageX-this.canvas.offset().left,
        y: this.lde.pageY-this.canvas.offset().top
      };
    }

    to = { x: lue.offsetX, y: lue.offsetY };
    if(lue.offsetX == undefined) {
      // Firefox bug
      to = {
        x: lue.pageX-this.canvas.offset().left,
        y: lue.pageY-this.canvas.offset().top
      };
    }

    layoutDrawPencil(this.ctx, from, to, this.currentColor, this.currentSize);

    this.socket.emit('drawPencil', {
      'from': from,
      'to': to,
      'color': this.currentColor,
      'size': this.currentSize
    });
  };

  this.toolListeners = function() {
    var manager = this;
    $('#drawing-tools > button').click(function(event){
      event.preventDefault();
      if($(this).attr('id') == 'clear') {
        manager.canvas.attr('data-current-tool', '');
        manager.clear();
      } else {
        var selectedActionTool = $(this).attr('id');
        if(manager.getCurrentTool() != selectedActionTool) {
          // A new tool is selected
          manager.canvas.attr('data-current-tool', selectedActionTool);
        } else {
          // A cancel tool is clicked
          manager.canvas.attr('data-current-tool', '');
        }
      }
    });
    $('#drawing-tools > #color').change(function(event){
      console.log($(this).val());
      manager.setCurrentColor($(this).val());
    });
    $('#drawing-tools > #size').change(function(event){
      console.log($(this).val());
      manager.setCurrentSize($(this).val());
    });
  };

  this.canvasListeners = function() {
    var manager = this;

    this.canvas.bind('vmouseup' ,function(e) {
      manager.setState('up');
      if(manager.getCurrentTool() != '') {
        if(manager.getCurrentTool() == 'line') {
          manager.drawLine(e);
        } else if(manager.getCurrentTool() == 'pen') {
          manager.drawPencil(e);
        }
      }
    });

    this.canvas.bind('vmousedown', function(e) {
      manager.setState('down');
      if(manager.getCurrentTool() != '') {
        manager.storeLastDownEvent(e);
      }
    });

    this.canvas.bind('vmousemove', function(e) {
      if(manager.getCurrentTool() == 'pen' && manager.state == 'down') {
        manager.drawPencil(e);
        manager.storeLastDownEvent(e);
      }
    });
  };

  this.remoteListeners = function() {
    var manager = this;

    this.socket.on('listenDrawPencil', function(data) {
      layoutDrawPencil(manager.ctx, data.from, data.to, data.color, data.size);
    });

    this.socket.on('listenDrawLine', function(data) {
      layoutDrawLine(manager.ctx, data.from, data.to, data.color, data.size);
    });

    this.socket.on('listenClear', function(data) {
      layoutClear(manager.ctx, manager.color, manager.width, manager.height);
    });
  };
};

function layoutDrawPencil(ctx, from, to, color, size) {
    ctx.save();
    ctx.beginPath();
    ctx.lineCap = "round";
    ctx.strokeStyle = color;
    ctx.lineWidth = size;
    ctx.moveTo(from.x, from.y);
    ctx.lineTo(to.x, to.y);
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
}

function layoutDrawLine(ctx, from, to, color, size) {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.lineWidth = size;
    ctx.moveTo(from.x, from.y);
    ctx.lineTo(to.x, to.y);
    ctx.stroke();
    ctx.closePath();
}

function layoutClear(ctx, color, width, height) {
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, width, height);
}
