var express = require('express');
var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

server.listen(8080);

app.use(express.static(__dirname + '/../../Svn'));

io.sockets.on('connection', function (socket) {
  
  socket.emit('Welcome', { hello: 'Let\'s start Drawing!'});
  
  socket.on('draw', function (data) {
    console.log(data);
  //   console.log(data);
  });
});