var express = require('express');
var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

server.listen(8080);

app.use(express.static(__dirname + '/../'));

io.sockets.on('connection', function (socket) {
  socket.emit('wellcome', { message: 'Let\'s start drawing' });

  socket.on('drawPencil', function(data){
    console.log(data);
    socket.broadcast.emit('listenDrawPencil', data);
  });

  socket.on('drawLine', function(data){
    console.log(data);
    socket.broadcast.emit('listenDrawLine', data);
  });

  socket.on('clear', function(data){
    console.log(data);
    socket.broadcast.emit('listenClear', data);
  });
});
