var express = require('express');
var app = express();

app.get('/hello.txt', function(req, res){
  console.log('url:'+req.url);
  var body	= "Hello World Txt";
  res.setHeader("Content-Type", "text/plain");
  res.setHeader("Content-Length", body.length);
  res.end(body);
});

app.get('/hello.html', function(req, res){
  console.log('url:'+req.url);
  var body	= "Hello World Html";
  res.end(body);
});


app.listen(8080);
console.log('Listening Port 8080');
